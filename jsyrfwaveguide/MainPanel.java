package jsyrfwaveguide;


import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.widget.attribute.NumberSpectrumViewer;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.Splash;
import fr.esrf.tangoatk.widget.util.chart.JLDataView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * RF Wave guide switches application
 */
public class MainPanel extends JFrame implements ActionListener {

  private static String RELEASE = "1.1";

  private boolean              runningFromShell;
  private TangoSynopticHandler tangoSynoptic;
  private AttributeList        attList;
  static  ErrorHistory         errWin;
  private JMenuBar             menuBar;
  private JMenu                fileMenu;
  private JMenuItem            fileExitMenu;
  private JMenu                viewMenu;
  private JMenuItem            viewErrorMenu;
  private JMenuItem            viewDiagMenu;

  private JPanel               innerPanel;
  private ModePanel            modePanel;
  private WGNetwork            wgNetwork;

  public MainPanel(boolean runningFromShell,String jdwFile) {

    this.runningFromShell = runningFromShell;

    // Splash window
    Splash splash = new Splash();
    splash.setTitle("SY Wave Guide Switches " + RELEASE);
    splash.setCopyright("(c) ESRF 2012");
    splash.setMaxProgress(2);

    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });

    // Create the menu
    menuBar = new JMenuBar();
    fileMenu = new JMenu("File");
    menuBar.add(fileMenu);
    fileExitMenu = new JMenuItem("Exit");
    fileExitMenu.addActionListener(this);
    fileMenu.add(fileExitMenu);
    viewMenu = new JMenu("View");
    menuBar.add(viewMenu);
    viewErrorMenu = new JMenuItem("Errors");
    viewErrorMenu.addActionListener(this);
    viewMenu.add(viewErrorMenu);
    viewDiagMenu = new JMenuItem("Diagnostic");
    viewDiagMenu.addActionListener(this);
    viewMenu.add(viewDiagMenu);
    setJMenuBar(menuBar);

    innerPanel = new JPanel();
    innerPanel.setLayout(new BorderLayout());

    // Create the tango synoptic handler
    errWin = new ErrorHistory();
    tangoSynoptic = new TangoSynopticHandler();
    tangoSynoptic.setErrorHistoryWindow(errWin);
    try {
      tangoSynoptic.setSynopticFileName(jdwFile);
    } catch (Exception e) {
      JOptionPane.showMessageDialog(this,"Failed to load "+jdwFile+"\n"+e.getMessage());
    }
    tangoSynoptic.setAutoZoom(true);
    tangoSynoptic.setBorder(BorderFactory.createLoweredBevelBorder());
    tangoSynoptic.getAttributeList().setRefreshInterval(3000);
    innerPanel.add(tangoSynoptic,BorderLayout.CENTER);

    splash.progress(1);

    // Create the mode panel
    attList = new AttributeList();
    attList.addErrorListener(errWin);

    modePanel = new ModePanel(attList);
    innerPanel.add(modePanel,BorderLayout.SOUTH);

    // General KeyListener
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      public void eventDispatched(AWTEvent event) {
        KeyEvent ke = (KeyEvent) event;
        if (ke.getID() == KeyEvent.KEY_PRESSED && ke.getKeyCode() == KeyEvent.VK_F5) {
          if (ke.isShiftDown()) {
            modePanel.toggleExpertMode();
          }
        }
      }
    }, AWTEvent.KEY_EVENT_MASK);

    // Create the WG network object
    // Compute path of the RF inside the wave guide network
    wgNetwork = new WGNetwork(tangoSynoptic);
    new Thread() {
      public void run() {
        while(true) {
          wgNetwork.computeWGStates();
          try {
            Thread.sleep(1000);
          } catch( InterruptedException e) {}
        }
      }
    }.start();

    setContentPane(innerPanel);

    splash.progress(2);
    attList.setRefreshInterval(3000);
    attList.startRefresher();
    ATKGraphicsUtils.centerFrameOnScreen(this);
    splash.setVisible(false);
    setTitle("SY Wave Guide Switches " + RELEASE);
    setVisible(true);

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==fileExitMenu ) {
      exitForm();
    } else if ( src==viewErrorMenu ) {
      ATKGraphicsUtils.centerFrameOnScreen(errWin);
      errWin.setVisible(true);
    } else if ( src==viewDiagMenu ) {
      fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }

  }

  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {
      System.exit(0);
    } else {
      tangoSynoptic.getAttributeList().stopRefresher();
      setVisible(false);
      dispose();
    }

  }

  public static void main(String[] args) {
    if(args.length!=1) {
      JOptionPane.showMessageDialog(null,"jsyrfwg: jdraw synoptic file name not specified");
    } else {
      new MainPanel(true,args[0]);
    }
  }

}
