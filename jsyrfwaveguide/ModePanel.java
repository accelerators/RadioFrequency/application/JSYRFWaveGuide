package jsyrfwaveguide;

import fr.esrf.Tango.DevFailed;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.core.attribute.AttributeFactory;
import fr.esrf.tangoatk.core.command.CommandFactory;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Panel for transmitter and wave guide mode
 */
public class ModePanel extends JPanel implements ModeComboListener, ActionListener, IDevStateScalarListener  {

  public final static Font borderFont = new Font("Dialog",Font.BOLD,12);

  StateViewer        SSA1_state;
  SimpleScalarViewer SSA1_mode;
  ModeComboBox       SSA1_Combo;
  INumberScalar      SSA1_mode_model;
  IDevStateScalar    SSA1_state_model;
  String             SSA1_stt = new String();

  StateViewer        SSA2_state;
  SimpleScalarViewer SSA2_mode;
  ModeComboBox       SSA2_Combo;
  INumberScalar      SSA2_mode_model;
  IDevStateScalar    SSA2_state_model;
  String             SSA2_stt = new String();

  StateViewer        SSA3_state;
  SimpleScalarViewer SSA3_mode;
  ModeComboBox       SSA3_Combo;
  INumberScalar      SSA3_mode_model;
  IDevStateScalar    SSA3_state_model;
  String             SSA3_stt = new String();

  StateViewer        SSA4_state;
  SimpleScalarViewer SSA4_mode;
  ModeComboBox       SSA4_Combo;
  INumberScalar      SSA4_mode_model;
  IDevStateScalar    SSA4_state_model;
  String             SSA4_stt = new String();

  BooleanColorViewer plcRemoteViewer;
  BooleanColorViewer enable220VViewer;
  BooleanColorViewer wgModeErrorViewer;
  BooleanColorViewer allOKViewer;
  BooleanColorViewer powerFailureViewer;
  BooleanColorViewer configErrorViewer;
  BooleanColorViewer expertModeViewer;

  JButton            sendBtn;
  ICommand           sendModel;

  boolean            enableWrite;
  boolean            expertMode = false;

  ModePanel(AttributeList attList) {

    setLayout(null);
    setPreferredSize(new Dimension(0,180));

    // Global -------------------------------------------------------------------------
    JPanel GlobalPanel = new JPanel();
    GlobalPanel.setLayout(null);
    GlobalPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Global",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    GlobalPanel.setBounds(5,120,1040,50);
    add(GlobalPanel);

    plcRemoteViewer = new BooleanColorViewer("PLC Remote");
    plcRemoteViewer.setBounds(10,20,120,25);
    try {
      plcRemoteViewer.setModel((IBooleanScalar)attList.add("sy/rfssa-wg/tra0/PLC_remote"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(plcRemoteViewer);

    enable220VViewer = new BooleanColorViewer("220V enable");
    enable220VViewer.setBounds(130,20,120,25);
    try {
      enable220VViewer.setModel((IBooleanScalar)attList.add("sy/rfssa-wg/tra0/Enable_220V"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(enable220VViewer);

    allOKViewer = new BooleanColorViewer("All OK");
    allOKViewer.setBounds(250, 20, 100, 25);
    allOKViewer.setFalseBackground(Color.RED);
    allOKViewer.setTrueBackground(Color.GREEN);
    try {
      allOKViewer.setModel((IBooleanScalar) attList.add("sy/rfssa-wg/tra0/All_OK"));
    } catch (ConnectionException e) {
    }
    GlobalPanel.add(allOKViewer);

    wgModeErrorViewer = new BooleanColorViewer("W.G. error");
    wgModeErrorViewer.setBounds(350, 20, 100, 25);
    wgModeErrorViewer.setFalseBackground(Color.GREEN);
    wgModeErrorViewer.setTrueBackground(Color.RED);
    try {
      wgModeErrorViewer.setModel((IBooleanScalar) attList.add("sy/rfssa-wg/tra0/WG_Error"));
    } catch (ConnectionException e) {
    }
    GlobalPanel.add(wgModeErrorViewer);

    powerFailureViewer = new BooleanColorViewer("Power Failure");
    powerFailureViewer.setBounds(450,20,120,25);
    powerFailureViewer.setFalseBackground(Color.GREEN);
    powerFailureViewer.setTrueBackground(Color.RED);
    try {
      powerFailureViewer.setModel((IBooleanScalar)attList.add("sy/rfssa-wg/tra0/Power_Failure"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(powerFailureViewer);

    configErrorViewer = new BooleanColorViewer("Config Error");
    configErrorViewer.setBounds(570,20,120,25);
    configErrorViewer.setFalseBackground(Color.GREEN);
    configErrorViewer.setTrueBackground(Color.RED);
    try {
      configErrorViewer.setModel((IBooleanScalar)attList.add("sy/rfssa-wg/tra0/ConfigError"));
    } catch (ConnectionException e ) {}
    GlobalPanel.add(configErrorViewer);

    expertModeViewer = new BooleanColorViewer("Expert mode");
    expertModeViewer.setToggleBackground(Color.WHITE);
    expertModeViewer.setBounds(690,20,120,25);
    GlobalPanel.add(expertModeViewer);

    sendBtn = new JButton("Send to PLC");
    sendBtn.setBounds(880,15,150,25);
    sendBtn.addActionListener(this);
    GlobalPanel.add(sendBtn);
    try {
      sendModel = CommandFactory.getInstance().getCommand("sy/rfssa-wg/tra0/send");
      sendModel.addErrorListener(ErrorPopup.getInstance());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/send",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/send",e2);
    }

    // SSA1 -----------------------------------------------------------------------------------------

    JPanel SSA1Panel = new JPanel();
    SSA1Panel.setLayout(null);
    SSA1Panel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "SSA-1",
                         TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                         borderFont, Color.BLACK) );
    add(SSA1Panel);

    JLabel SSA1SLab = new JLabel("State");
    SSA1SLab.setBounds(10,20,100,25);
    SSA1Panel.add(SSA1SLab);
    SSA1_state = new StateViewer();
    SSA1_state.setBackground(Color.WHITE);
    SSA1_state.setBounds(110, 20, 140, 25);
    try {
      SSA1_state.setModel((IDevStateScalar) attList.add("sy/rfssa-ampli/tra0-1/State"));
    } catch (ConnectionException e ) {}
    SSA1Panel.add(SSA1_state);

    JLabel SSA1MLab = new JLabel("Mode");
    SSA1MLab.setBounds(10,50,100,25);
    SSA1Panel.add(SSA1MLab);
    SSA1_mode = new SimpleScalarViewer();
    SSA1_mode.setBackgroundColor(Color.WHITE);
    SSA1_mode.setBounds(110, 50, 140, 25);
    SSA1Panel.add(SSA1_mode);
    try {
      SSA1_mode.setModel((IStringScalar) attList.add("sy/rfssa-wg/tra0/TRA0_1_mode_str"));
    } catch (ConnectionException e ) {}
    SSA1Panel.add(SSA1_mode);

    SSA1_Combo = new ModeComboBox();
    SSA1_Combo.addItem(new ModeComboItem("Dismount", 0));
    SSA1_Combo.addItem(new ModeComboItem("Cavity", 1));
    SSA1_Combo.addItem(new ModeComboItem("Dummy", 2));
    SSA1_Combo.addItem(new ModeComboItem("Cavity cond.", 3));
    SSA1_Combo.setBounds(110, 80, 140, 25);
    SSA1_Combo.addModeComboListener(this);
    SSA1Panel.add(SSA1_Combo);

    // SSA12 -----------------------------------------------------------------------------------------

    JPanel SSA2Panel = new JPanel();
    SSA2Panel.setLayout(null);
    SSA2Panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "SSA-2",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK));
    add(SSA2Panel);

    JLabel SSA2SLab = new JLabel("State");
    SSA2SLab.setBounds(10, 20, 100, 25);
    SSA2Panel.add(SSA2SLab);
    SSA2_state = new StateViewer();
    SSA2_state.setBackground(Color.WHITE);
    SSA2_state.setBounds(110, 20, 140, 25);
    try {
      SSA2_state.setModel((IDevStateScalar) attList.add("sy/rfssa-ampli/tra0-2/State"));
    } catch (ConnectionException e ) {}
    SSA2Panel.add(SSA2_state);

    JLabel SSA2MLab = new JLabel("Mode");
    SSA2MLab.setBounds(10, 50, 100, 25);
    SSA2Panel.add(SSA2MLab);
    SSA2_mode = new SimpleScalarViewer();
    SSA2_mode.setBackgroundColor(Color.WHITE);
    SSA2_mode.setBounds(110, 50, 140, 25);
    SSA2Panel.add(SSA2_mode);
    try {
      SSA2_mode.setModel((IStringScalar) attList.add("sy/rfssa-wg/tra0/TRA0_2_mode_str"));
    } catch (ConnectionException e ) {}
    SSA2Panel.add(SSA2_mode);

    SSA2_Combo = new ModeComboBox();
    SSA2_Combo.addItem(new ModeComboItem("Dismount", 0));
    SSA2_Combo.addItem(new ModeComboItem("Cavity", 1));
    SSA2_Combo.addItem(new ModeComboItem("Dummy", 2));
    SSA2_Combo.addItem(new ModeComboItem("Cavity cond.", 3));
    SSA2_Combo.setBounds(110, 80, 140, 25);
    SSA2_Combo.addModeComboListener(this);
    SSA2Panel.add(SSA2_Combo);

    // SSA21 -----------------------------------------------------------------------------------------

    JPanel SSA3Panel = new JPanel();
    SSA3Panel.setLayout(null);
    SSA3Panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "SSA-3",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK));
    add(SSA3Panel);

    JLabel SSA3SLab = new JLabel("State");
    SSA3SLab.setBounds(10, 20, 100, 25);
    SSA3Panel.add(SSA3SLab);
    SSA3_state = new StateViewer();
    SSA3_state.setBackground(Color.WHITE);
    SSA3_state.setBounds(110, 20, 140, 25);
    try {
      SSA3_state.setModel((IDevStateScalar) attList.add("sy/rfssa-ampli/tra0-3/State"));
    } catch (ConnectionException e ) {}
    SSA3Panel.add(SSA3_state);

    JLabel SSA3MLab = new JLabel("Mode");
    SSA3MLab.setBounds(10, 50, 100, 25);
    SSA3Panel.add(SSA3MLab);
    SSA3_mode = new SimpleScalarViewer();
    SSA3_mode.setBackgroundColor(Color.WHITE);
    SSA3_mode.setBounds(110, 50, 140, 25);
    SSA3Panel.add(SSA3_mode);
    try {
      SSA3_mode.setModel((IStringScalar) attList.add("sy/rfssa-wg/tra0/TRA0_3_mode_str"));
    } catch (ConnectionException e ) {}
    SSA3Panel.add(SSA3_mode);

    SSA3_Combo = new ModeComboBox();
    SSA3_Combo.addItem(new ModeComboItem("Dismount", 0));
    SSA3_Combo.addItem(new ModeComboItem("Cavity", 1));
    SSA3_Combo.addItem(new ModeComboItem("Dummy", 2));
    SSA3_Combo.addItem(new ModeComboItem("Cavity cond.", 3));
    SSA3_Combo.setBounds(110, 80, 140, 25);
    SSA3_Combo.addModeComboListener(this);
    SSA3Panel.add(SSA3_Combo);

    // SSA22 -----------------------------------------------------------------------------------------

    JPanel SSA4Panel = new JPanel();
    SSA4Panel.setLayout(null);
    SSA4Panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "SSA-4",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK));
    add(SSA4Panel);

    JLabel SSA4SLab = new JLabel("State");
    SSA4SLab.setBounds(10, 20, 100, 25);
    SSA4Panel.add(SSA4SLab);
    SSA4_state = new StateViewer();
    SSA4_state.setBackground(Color.WHITE);
    SSA4_state.setBounds(110, 20, 140, 25);
    try {
      SSA4_state.setModel((IDevStateScalar) attList.add("sy/rfssa-ampli/tra0-4/State"));
    } catch (ConnectionException e ) {}
    SSA4Panel.add(SSA4_state);

    JLabel SSA4MLab = new JLabel("Mode");
    SSA4MLab.setBounds(10, 50, 100, 25);
    SSA4Panel.add(SSA4MLab);
    SSA4_mode = new SimpleScalarViewer();
    SSA4_mode.setBackgroundColor(Color.WHITE);
    SSA4_mode.setBounds(110, 50, 140, 25);
    SSA4Panel.add(SSA4_mode);
    try {
      SSA4_mode.setModel((IStringScalar) attList.add("sy/rfssa-wg/tra0/TRA0_4_mode_str"));
    } catch (ConnectionException e ) {}
    SSA4Panel.add(SSA4_mode);

    SSA4_Combo = new ModeComboBox();
    SSA4_Combo.addItem(new ModeComboItem("Dismount", 0));
    SSA4_Combo.addItem(new ModeComboItem("Cavity", 1));
    SSA4_Combo.addItem(new ModeComboItem("Dummy", 2));
    SSA4_Combo.addItem(new ModeComboItem("Cavity cond.", 3));
    SSA4_Combo.setBounds(110, 80, 140, 25);
    SSA4_Combo.addModeComboListener(this);
    SSA4Panel.add(SSA4_Combo);

    // Place panels

    SSA1Panel.setBounds(5,5,260,115);
    SSA2Panel.setBounds(265, 5, 260, 115);
    SSA3Panel.setBounds(525, 5, 260, 115);
    SSA4Panel.setBounds(785, 5, 260, 115);

    // State reading
    try {
      SSA1_state_model = (IDevStateScalar)attList.add("sy/rfssa-ampli/tra0-1/State");
      SSA1_state_model.addDevStateScalarListener(this);
      SSA1_state_model.refresh();
    } catch (ConnectionException e ) {}

    try {
      SSA2_state_model = (IDevStateScalar)attList.add("sy/rfssa-ampli/tra0-2/State");
      SSA2_state_model.addDevStateScalarListener(this);
      SSA2_state_model.refresh();
    } catch (ConnectionException e ) {}

    try {
      SSA3_state_model = (IDevStateScalar)attList.add("sy/rfssa-ampli/tra0-3/State");
      SSA3_state_model.addDevStateScalarListener(this);
      SSA3_state_model.refresh();
    } catch (ConnectionException e ) {}

    try {
      SSA4_state_model = (IDevStateScalar)attList.add("sy/rfssa-ampli/tra0-4/State");
      SSA4_state_model.addDevStateScalarListener(this);
      SSA4_state_model.refresh();
    } catch (ConnectionException e ) {}

    // Initialise ModeCombo
    enableWrite = false;

    try {
      SSA1_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sy/rfssa-wg/tra0/TRA0_1_mode");
      SSA1_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      SSA1_mode_model.refresh();
      SSA1_Combo.setMode((int) SSA1_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_1_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_1_mode",e2);
    }

    try {
      SSA2_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sy/rfssa-wg/tra0/TRA0_2_mode");
      SSA2_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      SSA2_mode_model.refresh();
      SSA2_Combo.setMode((int) SSA2_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_2_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_2_mode",e2);
    }

    try {
      SSA3_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sy/rfssa-wg/tra0/TRA0_3_mode");
      SSA3_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      SSA3_mode_model.refresh();
      SSA3_Combo.setMode((int) SSA3_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_3_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_3_mode",e2);
    }

    try {
      SSA4_mode_model = (INumberScalar) AttributeFactory.getInstance().getAttribute("sy/rfssa-wg/tra0/TRA0_4_mode");
      SSA4_mode_model.addSetErrorListener(ErrorPopup.getInstance());
      SSA4_mode_model.refresh();
      SSA4_Combo.setMode((int) SSA4_mode_model.getNumberScalarValue());
    } catch (DevFailed e1) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_4_mode",e1);
    } catch (ConnectionException e2) {
      ErrorPane.showErrorMessage(this,"sy/rfssa-wg/tra0/TRA0_4_mode",e2);
    }

    enableWrite = true;

  }

  public void modeChange(ModeComboBox src) {

    if( src== SSA1_Combo) {
      traModeChange();
    } else if ( src== SSA2_Combo) {
      traModeChange();
    } else if ( src== SSA3_Combo) {
      traModeChange();
    } else if ( src== SSA4_Combo) {
      traModeChange();
    }

  }

  private void traModeChange() {

    SSA1_Combo.enableAll();
    SSA2_Combo.enableAll();
    SSA3_Combo.enableAll();
    SSA4_Combo.enableAll();

    int tra1Mode = SSA1_Combo.getMode();
    int tra2Mode = SSA2_Combo.getMode();
    int tra3Mode = SSA3_Combo.getMode();
    int tra4Mode = SSA4_Combo.getMode();

    // Write mode to the DS
    if( enableWrite ) {
      SSA1_mode_model.setValue((double) tra1Mode);
      SSA2_mode_model.setValue((double) tra2Mode);
      SSA3_mode_model.setValue((double) tra3Mode);
      SSA4_mode_model.setValue((double) tra4Mode);
    }

  }

  public void devStateScalarChange(DevStateScalarEvent evt) {

    Object src = evt.getSource();
    String state = evt.getValue();

    if (src == SSA1_state_model) {
      SSA1_stt = state;
    } else if (src == SSA2_state_model) {
      SSA2_stt = state;
    } else if (src == SSA3_state_model) {
      SSA3_stt = state;
    } else if (src == SSA4_state_model) {
      SSA4_stt = state;
    }

    enablePanelItem();

  }

  public void stateChange(AttributeStateEvent e)
  {
  }

  public void errorChange(ErrorEvent evt)
  {

    Object src = evt.getSource();

    if( src== SSA1_state_model) {
      SSA1_stt = IDevice.UNKNOWN;
    } else if ( src== SSA2_state_model) {
      SSA2_stt = IDevice.UNKNOWN;
    } else if ( src== SSA3_state_model) {
      SSA3_stt = IDevice.UNKNOWN;
    } else if ( src== SSA4_state_model) {
      SSA4_stt = IDevice.UNKNOWN;
    }

    enablePanelItem();

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if(src==sendBtn) {
      SendToPLC();
    }

  }

  private void SendToPLC() {

    sendModel.execute();

  }

  public void toggleExpertMode() {

    if( expertMode ) {

      expertMode=false;
      expertModeViewer.setToggleBackground(Color.WHITE);
      
    } else {

      if( JOptionPane.showConfirmDialog(null,"You will have access to all menus.\n"
                                             +"Do you want to switch to expert mode ?",
                                             "Expert Mode Confirmation",JOptionPane.YES_NO_OPTION)==JOptionPane.NO_OPTION) {
        return;
      }

      expertMode=true;
      expertModeViewer.setToggleBackground(Color.ORANGE);

    }
    enablePanelItem();

  }

  private void enablePanelItem() {

    sendBtn.setEnabled(true);

    // Check transmitter mode
    if (!expertMode) {

      if (SSA1_stt.equalsIgnoreCase(IDevice.ON) ||
          SSA1_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        SSA1_Combo.setEnabled(false);
      } else {
        SSA1_Combo.setEnabled(true);
      }

      if (SSA2_stt.equalsIgnoreCase(IDevice.ON) ||
          SSA2_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        SSA2_Combo.setEnabled(false);
      } else {
        SSA2_Combo.setEnabled(true);
      }

      if (SSA3_stt.equalsIgnoreCase(IDevice.ON) ||
          SSA3_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        SSA3_Combo.setEnabled(false);
      } else {
        SSA3_Combo.setEnabled(true);
      }

      if (SSA4_stt.equalsIgnoreCase(IDevice.ON) ||
          SSA4_stt.equalsIgnoreCase(IDevice.ALARM) ) {
        SSA4_Combo.setEnabled(false);
      } else {
        SSA4_Combo.setEnabled(true);
      }

    } else {

      // Expert mode
      SSA1_Combo.setEnabled(true);
      SSA2_Combo.setEnabled(true);
      SSA3_Combo.setEnabled(true);
      SSA4_Combo.setEnabled(true);

    }

  }

}
