package jsyrfwaveguide;

import java.util.EventListener;

/**
 * Interface of the ModeCombo
 */
public interface ModeComboListener extends EventListener {

      /**
       * Invoked when a mode is selected
       */
      public void modeChange(ModeComboBox src);

 }
