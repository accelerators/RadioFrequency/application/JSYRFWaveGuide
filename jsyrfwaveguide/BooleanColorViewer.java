package jsyrfwaveguide;

import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.widget.util.ATKConstant;

import javax.swing.*;
import java.awt.*;


/** An BooleanColorViewer is a BooleanScalar attribute viewer where you can
 * choose the color for the true or false value.
 *
 */
public class BooleanColorViewer extends JPanel implements IBooleanScalarListener
{

  private IBooleanScalar   model=null;
  private Color            trueBackground;
  private Color            falseBackground;
  private JPanel           btnPanel;
  private JPanel           innerPanel;
  private JLabel           label;

  // ---------------------------------------------------
  // Construction
  // ---------------------------------------------------

  public BooleanColorViewer(String title)
  {
    super();
    setLayout(null);
    label = new JLabel();
    label.setText(title);
    add(label);
    trueBackground = Color.GREEN;
    falseBackground = Color.WHITE;
    btnPanel = new JPanel();
    btnPanel.setLayout(null);
    btnPanel.setBorder(BorderFactory.createLoweredBevelBorder());
    add(btnPanel);
    innerPanel = new JPanel();
    innerPanel.setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
    btnPanel.add(innerPanel);

  }

  public void setTrueBackground(Color c) {
    trueBackground = c;
    repaint();
  }

  public void setFalseBackground(Color c) {
    falseBackground = c;
    repaint();
  }

  public void setToggleBackground(Color c) {
    innerPanel.setBackground(c);
  }

  // ---------------------------------------------------
  // Model stuff
  // ---------------------------------------------------

  public IBooleanScalar getModel()
  {
     return model;
  }

  public void setModel(IBooleanScalar boolModel) {

    if (model != null) {
      model.removeBooleanScalarListener(this);
      model = null;
    }

    if (boolModel != null) {
      model = boolModel;
      model.addBooleanScalarListener(this);
    }

  }

  public void clearModel()
  {
    setModel(null);
  }

  // ---------------------------------------------------
  // IBooleanScalarListener listener
  // ---------------------------------------------------

  public void booleanScalarChange(BooleanScalarEvent e)
  {
    boolean value = e.getValue();
    if(value) {
      innerPanel.setBackground(trueBackground);
    } else {
      innerPanel.setBackground(falseBackground);
    }
  }

  public void stateChange(AttributeStateEvent evt)
  {
  }

  public void errorChange(ErrorEvent evt)
  {
    innerPanel.setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
  }

  // ---------------------------------------------------------
  // Placement
  // ---------------------------------------------------------

  public void setBounds(int x,int y,int width,int height) {
    super.setBounds(x,y,width,height);
    btnPanel.setBounds(0,6,13,13);
    innerPanel.setBounds(2,2,9,9);
    label.setBounds(17,0,width-15,25);
  }

}
