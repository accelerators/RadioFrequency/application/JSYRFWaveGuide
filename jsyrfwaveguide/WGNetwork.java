package jsyrfwaveguide;

import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import fr.esrf.tangoatk.widget.util.jdraw.JDrawEditor;

import java.awt.*;
import java.util.Vector;


class NetWorkItem {

  NetWorkItem() {
    wg = null;
    wgSwitch = null;
    next1 = null;
    next2 = null;
  }

  JDObject wg;           // The wave guide
  JDObject wgSwitch;     // Wage guide switch (at the end of the wg)

  NetWorkItem next1;     // The next wage guide (wg_switch position 1)
  NetWorkItem next2;     // The next waveguide  (wg_switch position 2)

}

/**
 * A class that compute RF path in the wage guide network
 */
public class WGNetwork {

  JDrawEditor jde;

  // Starting node
  NetWorkItem SSA1;
  NetWorkItem SSA2;
  NetWorkItem SSA3;
  NetWorkItem SSA4;

  WGNetwork(JDrawEditor jde) {

    this.jde = jde;

    // Construct the network wave guide

    NetWorkItem S11_CAVSY = new NetWorkItem();
    S11_CAVSY.wg = getObjectByName("w_S11_CAVSY");

    NetWorkItem S12_D = new NetWorkItem();
    S12_D.wg = getObjectByName("w_S12_D");

    SSA1 = new NetWorkItem();
    SSA1.wg = getObjectByName("w_ssa1_S1");
    SSA1.wgSwitch = getObjectByName("sy/rfssa-wg/tra0/S1_Position");
    SSA1.next1 = S11_CAVSY;
    SSA1.next2 = S12_D;


    NetWorkItem S21_CAVSY = new NetWorkItem();
    S21_CAVSY.wg = getObjectByName("w_S21_CAVSY");

    NetWorkItem S22_D = new NetWorkItem();
    S22_D.wg = getObjectByName("w_S22_D");

    SSA2 = new NetWorkItem();
    SSA2.wg = getObjectByName("w_ssa2_S2");
    SSA2.wgSwitch = getObjectByName("sy/rfssa-wg/tra0/S2_Position");
    SSA2.next1 = S21_CAVSY;
    SSA2.next2 = S22_D;


    NetWorkItem S31_CAVSY = new NetWorkItem();
    S31_CAVSY.wg = getObjectByName("w_S31_CAVSY");

    NetWorkItem S32_D = new NetWorkItem();
    S32_D.wg = getObjectByName("w_S32_D");

    SSA3 = new NetWorkItem();
    SSA3.wg = getObjectByName("w_ssa3_S3");
    SSA3.wgSwitch = getObjectByName("sy/rfssa-wg/tra0/S3_Position");
    SSA3.next1 = S31_CAVSY;
    SSA3.next2 = S32_D;

    NetWorkItem S41_CAVSY = new NetWorkItem();
    S41_CAVSY.wg = getObjectByName("w_S41_CAVSY");

    NetWorkItem S42_D = new NetWorkItem();
    S42_D.wg = getObjectByName("w_S42_D");

    SSA4 = new NetWorkItem();
    SSA4.wg = getObjectByName("w_ssa4_S4");
    SSA4.wgSwitch = getObjectByName("sy/rfssa-wg/tra0/S4_Position");
    SSA4.next1 = S41_CAVSY;
    SSA4.next2 = S42_D;

  }

  private JDObject getObjectByName(String name) {

    Vector v = jde.getObjectsByName(name,false);

    if( v.size()==0 ) {
      System.out.println("Warning: object "+name+" not found");
      return null;
    }

    if(v.size()>1) {
      System.out.println("Warning: several objects have the same name: "+name);
    }

    return (JDObject)v.get(0);

  }

  public void computeWGStates() {

    resetNetwork();
    browseNetwork(SSA1);
    browseNetwork(SSA2);
    browseNetwork(SSA3);
    browseNetwork(SSA4);
    jde.repaint();

  }

  private void resetNetwork() {

    for(int i=0;i<jde.getObjectNumber();i++) {
      JDObject o = jde.getObjectAt(i);
      if(o.getName().startsWith("w_")) {
        o.setBackground(Color.WHITE);
      }
    }

  }

  private void browseNetwork(NetWorkItem n) {

    n.wg.setBackground(Color.GREEN);

    if(n.wgSwitch!=null) {

      int v = n.wgSwitch.getValue();
      if(v==1 && n.next1!=null) browseNetwork(n.next1);
      if(v==2 && n.next2!=null) browseNetwork(n.next2);

    } else {

      // Special case for the magic tee
      if(n.next1!=null && n.next2!=null) {
        browseNetwork(n.next1);
        browseNetwork(n.next2);
      }

    }
    
  }

}
