/*
 *  Copyright (C) :	2009
 *			European Synchrotron Radiation Facility
 *			BP 220, Grenoble 38043
 *			FRANCE
 * 
 *  This file is part of Tango.
 * 
 *  Tango is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  Tango is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Tango.  If not, see <http://www.gnu.org/licenses/>.
 */
 
package jsyrfwaveguide;


import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.DevStateScalarEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IDevStateScalarListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.IDeviceApplication;
import fr.esrf.tangoatk.widget.device.IDevicePopUp;
import fr.esrf.tangoatk.widget.device.SingletonStatusViewer;
import fr.esrf.tangoatk.widget.util.*;
import java.awt.GridBagConstraints;
import javax.swing.*;

public class StateViewer extends JSmoothLabel implements IDevStateScalarListener
{

    protected IDevStateScalar   model=null;
    private String            currentState = "UNKNOWN";

    /**
    * Contructs a SimpleStateViewer.
    */
    public StateViewer()
    {
        setFont(ATKConstant.labelFont);
        setOpaque(true);
        setState(IDevice.UNKNOWN);
        setBorder(BorderFactory.createEtchedBorder());
    }

    public void setModel(IDevStateScalar stateAtt)
    {

        if (model != null) clearModel();
        if (stateAtt == null) return;
        model = stateAtt;
        model.addDevStateScalarListener(this);
        model.refresh();

    }

    public void clearModel()
    {
        if (model != null)
        {
            model.removeDevStateScalarListener(this);
            model = null;
            setState(IDevice.UNKNOWN);
        }
    }

    /**
    * <code>setState</code>
    *
    * @param state a <code>String</code> value
    */
    private void setState(String state)
    {
        currentState = state;
        setText(currentState);
    }


    public void devStateScalarChange(DevStateScalarEvent evt)
    {
        setState(evt.getValue());
    }

    public void stateChange(AttributeStateEvent e)
    {
    }

    public void errorChange(ErrorEvent evt)
    {
        setState(IDevice.UNKNOWN);
    }

}
