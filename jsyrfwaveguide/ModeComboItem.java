package jsyrfwaveguide;

/**
 * Item for the ModeComboBox
 */
public class ModeComboItem {

  private boolean isDisabled;
  private String  name;
  private int     mode;

  ModeComboItem(String name,int mode) {
    this.name = name;
    this.mode = mode;
    this.isDisabled = false;
  }

  public void setDisabled(boolean disabled) {
    isDisabled = disabled;
  }

  public boolean isDisabled() {
    return isDisabled;
  }

  public int getMode() {
    return mode;
  }

  public String toString() {
    return name;
  }

}
