package jsyrfwaveguide;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.EventListenerList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A ComboBox that supports disabled item
 */

class ComboRenderer extends JLabel implements ListCellRenderer {

  public ComboRenderer() {
    setOpaque(true);
    setBorder(new EmptyBorder(1, 1, 1, 1));
  }

  public Component getListCellRendererComponent(JList list, Object value,
                                                int index, boolean isSelected, boolean cellHasFocus) {
    if (isSelected) {
      setBackground(list.getSelectionBackground());
      setForeground(list.getSelectionForeground());
    } else {
      setBackground(list.getBackground());
      setForeground(list.getForeground());
    }
    if (((ModeComboItem) value).isDisabled()) {
      setBackground(list.getBackground());
      setForeground(UIManager.getColor("Label.disabledForeground"));
    }
    setFont(list.getFont());
    setText(value.toString());
    return this;
  }

}

public class ModeComboBox extends JComboBox implements ActionListener {

  private EventListenerList listenerList;
  private ModeComboItem     currentItem;

  ModeComboBox() {
    super();
    setRenderer(new ComboRenderer());
    setEditable(false);
    addActionListener(this);
    listenerList = new EventListenerList();
  }

  public void enableAll() {
    for(int i=0;i<getItemCount();i++) {
      ModeComboItem it = (ModeComboItem)getItemAt(i);
      it.setDisabled(false);
    }
  }

  public void disableItem(int mode) {
    for(int i=0;i<getItemCount();i++) {
      ModeComboItem it = (ModeComboItem)getItemAt(i);
      if(it.getMode()==mode) it.setDisabled(true);
    }
  }

  public void setMode(int mode) {

    boolean found=false;
    int i=0;
    while(!found && i<getItemCount()) {
      found = ((ModeComboItem)getItemAt(i)).getMode() == mode;
      if(!found) i++;
    }
    if(found) setSelectedIndex(i);

  }

  public int getMode() {

    ModeComboItem it = (ModeComboItem)getSelectedItem();
    return it.getMode();
    
  }

  public void actionPerformed(ActionEvent e) {

    ModeComboItem tempItem = (ModeComboItem)getSelectedItem();
    if (tempItem.isDisabled()) {
      setSelectedItem(currentItem);
    } else {
      currentItem = tempItem;
      fireValueChange(this);
    }

  }

  public void addModeComboListener(ModeComboListener l) {
    listenerList.add(ModeComboListener.class, l);
  }

  private void fireValueChange(ModeComboBox src) {

    ModeComboListener[] list = listenerList.getListeners(ModeComboListener.class);

    for (int i = 0; i < list.length; i++) {
        list[i].modeChange(src);
    }

  }

}
